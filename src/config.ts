export const SITE = {
	name: 'Forgejo',

	origin: 'https://forgeowo.codeberg.page/',
	basePathname: '/',
	trailingSlash: true,

	title: 'Fwowgejwo – Beyond cwoding UwU We fwowge owo',
	description:
		'Fwowgejwo is a sewf-hwosted wightweight swoftwawe fwowge. Easy two instaww and wow maintenyance ^w^ it just dwoes teh jwob owo',
};

export const BLOG = {
	disabled: false,
	postsPerPage: 5,

	blog: {
		disabled: false,
		pathname: 'news', // blog main path, you can change this to "articles" (/articles)
	},

	post: {
		disabled: false,
		pathname: '', // empty for /some-post, value for /pathname/some-post
	},

	category: {
		disabled: false,
		pathname: 'category', // set empty to change from /category/some-category to /some-category
	},

	tag: {
		disabled: false,
		pathname: 'tag', // set empty to change from /tag/some-tag to /some-tag
	},
};

export const DOCS = {
	repo: 'https://codeberg.org/forgejo/docs',
};
