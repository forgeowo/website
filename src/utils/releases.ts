import { readFileSync } from 'fs';

import { SemVer } from 'semver';

import { getCollection, type CollectionEntry } from 'astro:content';

import { giteaApi, type Release } from 'gitea-js';
const api = giteaApi('https://codeberg.org', {});

let _releases: Release[];
let _releaseBlogPosts: CollectionEntry<'blog'>[];

export class ReleaseVersion extends SemVer {
	readonly release?: Release;

	constructor(release?: Release) {
		super(release?.tag_name || '0.0.0');
		this.release = release;
	}

	majorMinor() {
		return `${this.major}.${this.minor}`;
	}

	docsTag() {
		return `v${this.major}.${this.minor}`;
	}
}

const _latestReleases = {
	stable: new ReleaseVersion(),
	lts: new ReleaseVersion(),
};

interface LtsRelease {
	major: string | number;
	minor: string | number;
	cut: string;
	release: string;
	eol: string;
	lts: boolean;
}

/** Fetch a list of the latest Forgejo releases from Codeberg using the Forgejo API. */
export const fetchReleases = async () => {
	await ensureReleasesCached();
	return _releases;
};

async function ensureReleasesCached() {
	if (_releases) {
		return;
	}
	_releases = [];
	for (let page = 1; ; page++) {
		const response = await api.repos.repoListReleases('forgejo', 'forgejo', {
			page: page,
			limit: 1000,
			'pre-release': false,
		});
		if (response.data.length > 0) {
			_releases.push(...response.data);
		} else {
			break;
		}
	}
	const releaseScheduleJSON = readFileSync('forgejo-docs/next/release-schedule.json', 'utf8');
	const releaseSchedule = JSON.parse(releaseScheduleJSON);
	const ltsReleases = Object.fromEntries(releaseSchedule.map((x: LtsRelease) => [`${x.major}.${x.minor}`, x.lts]));

	_releases.forEach((release) => {
		const rv = new ReleaseVersion(release);

		if (rv.compare(_latestReleases.stable) > 0) {
			_latestReleases.stable = rv;
		}

		const majorMinor = rv.majorMinor();
		if (majorMinor in ltsReleases && ltsReleases[majorMinor] && rv.compare(_latestReleases.lts) > 0) {
			_latestReleases.lts = rv;
		}
	});
}

/** Get the latest Forgejo release from Codeberg using the Forgejo API. */
export const getLatestReleaseVersion = async () => {
	await ensureReleasesCached();
	const releaseVersion = _latestReleases.stable;
	if (!releaseVersion?.release) {
		throw new Error('no stable release!');
	}
	return releaseVersion;
};

export const releaseLabel = async (release: Release) => {
	await ensureReleasesCached();
	switch (release.name) {
		case _latestReleases.stable.release?.name:
			return 'Stable';
		case _latestReleases.lts.release?.name:
			return 'LTS';
	}
	return '';
};

export const getReleaseNotesUrl = async (rv: ReleaseVersion) => {
	if (rv.major >= 9 || (rv.major == 8 && rv.patch > 0) || (rv.major == 7 && rv.patch > 6)) {
		return `https://codeberg.org/forgejo/forgejo/src/branch/forgejo/release-notes-published/${rv.version}.md`;
	} else {
		const versionSlug = rv.version.replaceAll('.', '-');
		return `https://codeberg.org/forgejo/forgejo/src/branch/forgejo/RELEASE-NOTES.md#${versionSlug}`;
	}
};

/** Get the blog post for a release, if there is one. */
export const getReleaseBlogPost = async (rv: ReleaseVersion) => {
	if (!_releaseBlogPosts) {
		_releaseBlogPosts = await getCollection('blog', ({ data }) => data.release);
	}
	return _releaseBlogPosts.find(({ data }) => data.release == rv.version);
};
