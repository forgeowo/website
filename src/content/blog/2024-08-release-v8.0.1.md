---
title: Forgejo Security Release v8.0.1 & v7.0.7
publishDate: 2024-08-09
tags: ['releases', 'security']
release: 8.0.1
excerpt: 'The Forgejo v8.0.1 & v7.0.7 releases contain a security fix for a cross-site scripting (XSS) vulnerability that allowed repository owners to create links that executed javascript when clicking on them.'
---

Forgejo [v8.0.1](https://codeberg.org/forgejo/forgejo/releases/tag/v8.0.1) & [v7.0.7](https://codeberg.org/forgejo/forgejo/releases/tag/v7.0.7) was released 9 August 2024.

These releases contain a fix for a security issue, which can be exploited by registered Forgejo users who can change the description of a repository. A [change introduced in Forgejo v1.21](https://codeberg.org/forgejo/forgejo/pulls/1433) allows a Forgejo user with write permission on a repository description [to inject a client-side script into the web page viewed by the visitor](https://en.wikipedia.org/wiki/Cross-site_scripting). This XSS vulnerability allows for `href` in anchor elements to be set to a `javascript:` URI in the repository description, which will execute the specified script upon clicking (and not upon loading). [`AllowStandardURLs`](https://pkg.go.dev/github.com/microcosm-cc/bluemonday#Policy.AllowStandardURLs) is now called for the repository description policy, which ensures that URIs in anchor elements are `mailto:`, `http://` or `https://` thereby disallowing the `javascript:` URI.

### Recommended Action

We _strongly recommend_ that all Forgejo installations are upgraded to the latest version as soon as possible.

### Contribute to Forgejo

If you have any feedback or suggestions for Forgejo, we'd love to hear from you! Open an issue on [our issue tracker](https://codeberg.org/forgejo/forgejo/issues) for feature requests or bug reports. You can also find us [on the Fediverse](https://floss.social/@forgejo), or drop by [our Matrix space](https://matrix.to/#/#forgejo:matrix.org) ([main chat room](https://matrix.to/#/#forgejo-chat:matrix.org)) to say hi!
