---
title: Non-free dependency discovered in Forgejo and removed
publishDate: 2024-07-30
tags: ['report']
excerpt: A non-free JavaScript library was found in the project's dependency structure, and the entire component that relied on it was re-implemented. The new versions 8.0.0 and 7.0.6 will be released without this library. This step is important to meet the core values of Forgejo.
---

On 18 July 2024, a [small piece of non Free Software was discovered](https://codeberg.org/forgejo/forgejo/issues/4569) within the Forgejo codebase.
It is only used to display the top authors contribution graph (which is part of the [repository activity](https://codeberg.org/forgejo/forgejo/activity)) in the web interface.
A replacement [was implemented](https://codeberg.org/forgejo/forgejo/pulls/4571) and merged on 20 July 2024.
This piece of non-Free Software is no longer contained in the [v8.0.0 release](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/RELEASE-NOTES.md#8-0-0) and the [v7.0.6 point release](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/RELEASE-NOTES.md#7-0-6).

---

During a discussion about the future of Forgejo's license, it was discovered that a non-free dependency of a dependency initially created for Gitea was loaded into the project.

The usage of the non-free dependency was [reported](https://codeberg.org/forgejo/forgejo/issues/4569) in the main issue tracker on 18 July.
A few hours later, a [pull request](https://codeberg.org/forgejo/forgejo/pulls/4571) was opened to remove the dependency.
In addition, [a discussion](https://codeberg.org/forgejo/discussions/issues/193) was created to track the problem and the resulting consequences as a whole.
The pull request was merged just over one day after the initial submission.

The [dependency already existed](https://github.com/go-gitea/gitea/commit/81cfe243f9cb90b0a75de7a03bb2d264c97f0036#diff-7ae45ad102eab3b6d7e7896acd08c427a9b25b346470d7bc6507b6481575d519R9) at the time of the fork from Gitea and was therefore included in Forgejo from the beginning.
The commitment of Forgejo is to always be [free as in freedom, open source and a community-first product](https://codeberg.org/forgejo/governance/src/branch/main/MISSION.md#values).
Non-free dependencies and distribution licenses are incompatible with the values of Forgejo.
Therefore, it was of high importance to remove the problematic dependency.

The [release of 8.0.0](/2024-06-release-v8-0/) was therefore blocked until the problem was solved.
The removal of the binary was also ported to [7.0.6](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/RELEASE-NOTES.md#7-0-6).

Additionally [the author](https://github.com/lafriks/vue-bar-graph/issues/14) of the dependency and [Gitea](https://github.com/go-gitea/gitea/issues/31660) were informed of the non-free subdependency.

In order to rule out further infringements, an [improved tool was introduced](https://codeberg.org/forgejo/forgejo/pulls/4574) to check the licenses of all dependencies.
It runs in the CI, and fails if an incompatibility is found.
Due to the new tool which works more precisely, it can lead to more licenses being included in the [`license.txt`](https://next.forgejo.org/assets/licenses.txt) - also from dependencies that are removed in the build process.
But better this safe way than missing licenses from dependencies in the end.

Because [GSAP](https://gsap.com/community/standard-license/), the indirect dependency, is not Free Software, it cannot be [distributed in the Forgejo organization](https://codeberg.org/forgejo) hosted by Codeberg.
It is prohibited by the [Codeberg Terms of Use](https://codeberg.org/Codeberg/org/src/branch/main/TermsOfUse.md#2-allowed-content-usage) and goes against the [Forgejo core values](https://codeberg.org/forgejo/governance/src/branch/main/MISSION.md#values).
**The Forgejo binaries and container images will be deleted.**
It will take some time, since the technical impact on existing Forgejo instances that depend on them has to be carefully addressed.

---

During the investigation, two other indirect dependencies with incompatible licenses were found.

One is a dependency which was used for citing a repository in APA format (if the repository is set up for this) and has been [removed for the moment](https://codeberg.org/forgejo/forgejo/pulls/4595).
It has a more restrictive, [copyleft license](https://github.com/Juris-M/citeproc-js/blob/master/LICENSE) which is incompatible with the [current license](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/LICENSE) of Forgejo.
Repositories can therefore currently only be cited in the widely used BibTeX format.
As Forgejo decided to accept copyleft license last year, this dependency may be added again in the future.

The other is [elkjs](https://github.com/kieler/elkjs) included by [Mermaid](https://github.com/mermaid-js/mermaid).
It also has a more restrictive, [copyleft license](https://github.com/kieler/elkjs/blob/master/LICENSE.md) which is incompatible with the [current license](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/LICENSE) of Forgejo.
Since [elk as renderer is experimental](https://mermaid.js.org/syntax/flowchart.html?#renderer) so far, it was decided to [remove elk manually](https://codeberg.org/forgejo/forgejo/pulls/4670).
If you decide to set elkjs specifically as a renderer, an error now occurs.
This is currently the only solution for the license issue.
