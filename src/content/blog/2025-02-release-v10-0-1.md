---
title: Forgejo Security Releases v10.0.1 and v7.0.13
publishDate: 2025-02-08
tags: ['releases', 'security']
release: 10.0.1
excerpt: The Forgejo v10.0.1 and v7.0.13 releases contain critical security fixes related to permissions enforcement of web endpoints.
---

[Forgejo v10.0.1](https://codeberg.org/forgejo/forgejo/releases/tag/v10.0.1) and [Forgejo v7.0.13](https://codeberg.org/forgejo/forgejo/releases/tag/v7.0.13) were released 8 February 2025.

This release fixes permissions enforcement of Forgejo Actions and projects.

This release also contains other bug fixes, as detailed [in the corresponding milestone](https://codeberg.org/forgejo/forgejo/milestone/9468).

### Recommended Action

We _strongly recommend_ that all Forgejo installations are upgraded to the latest version as soon as possible.

### Impact

These security issues can be exploited:

- by users who are registered on the instance, to delete Forgejo Actions runners and variables or modify variables.
- to get the titles, authors, labels and creation dates of issues or pull requests in private repositories, when they are referenced by a project from the containing user or organization.

### Forgejo Actions web endpoints vulnerable to manually crafted identifiers

Some Forgejo Actions related web endpoints, such as deleting a [Forgejo Actions variable](https://forgejo.org/docs/next/user/actions/#variables), rely on an identifier unique to an object (a variable in this example).

The permissions required for the user performing the action on the
repository are properly enforced. But a check was missing to ensure
that the object (a variable in the example) also belongs to the
repository the permissions are checked against. Without this check it
was possible both to perform destructive actions (runners and
variables) or to modify variables in repositories unrelated to the
request, including private ones.

The vulnerable endpoints were fixed and tests were added to verify the fixes are effective.

### User or organization wide projects leaking information about private issues or pull requests

If a project is created in a user or an organization, it can be used to display some information about issues or pull requests extracted from the repositories they contain:

- title
- author
- creation date
- labels
- URL to the issue or pull request

When a publicly readable user or organization contains a private
repository, a user with access to this private repository can add an
issue or pull request to the publicly available project. A user who
was not allowed to read the private repository was able to see the
information about the issue or pull request displayed in the
project. When this same user visits the URL of the issue or pull
request, they are denied access because they do not have the required permissions.

The vulnerable web endpoints were fixed and tests written to verify the fix is effective.

### Forgejo gives advance warning of security releases

Similar to what is done when a Go release contains a security fix,
Forgejo publishes advance warning of security releases. They
do not reveal the details of the vulnerability but will allow
Forgejo admins to plan ahead and better secure their instance. Anyone
can watch the
[dedicated tracker](https://codeberg.org/forgejo/security-announcements/) or
[subscribe to the RSS feed](https://codeberg.org/forgejo/security-announcements.rss).

Third parties may also get more information ahead of time when they agree to comply with
the [Forgejo Security Policy](https://codeberg.org/forgejo/governance/src/commit/48c2d7fe8e5f7e43ec40e39e7081e2b3a8f79978/SECURITY-POLICY.md#forgejo-security-policy).

### Contribute to Forgejo

If you have any feedback or suggestions for Forgejo, we'd love to hear from you! Open an issue on [our issue tracker](https://codeberg.org/forgejo/forgejo/issues) for feature requests or bug reports. You can also find us [on the Fediverse](https://floss.social/@forgejo), or drop by [our Matrix space](https://matrix.to/#/#forgejo:matrix.org) ([main chat room](https://matrix.to/#/#forgejo-chat:matrix.org)) to say hi!
