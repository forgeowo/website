import astroParser from 'astro-eslint-parser';
import eslint from '@eslint/js';
import eslintConfigPrettier from 'eslint-config-prettier';
import eslintPluginAstro from 'eslint-plugin-astro';
import tseslint from 'typescript-eslint';

export default [
	eslint.configs.recommended,
	...tseslint.configs.recommended,
	...eslintPluginAstro.configs.recommended,
	eslintConfigPrettier,

	{
		ignores: [
			'**/dist',
			'**/node_modules',
			'**/.github',
			'**/.astro',
			'src/env.d.ts',
			'**/forgejo-docs',
			'**/.pnpm-store',
		],
	},

	{
		files: ['**/*.js'],

		rules: {
			'no-mixed-spaces-and-tabs': ['error', 'smart-tabs'],
		},
	},

	{
		files: ['**/*.astro'],

		languageOptions: {
			parser: astroParser,
			ecmaVersion: 5,
			sourceType: 'script',

			parserOptions: {
				parser: tseslint.parser,
				extraFileExtensions: ['.astro'],
			},
		},

		rules: {
			'no-mixed-spaces-and-tabs': ['error', 'smart-tabs'],
		},
	},

	{
		files: ['**/*.ts'],

		rules: {
			'@typescript-eslint/no-unused-vars': [
				'error',
				{
					argsIgnorePattern: '^_',
					destructuredArrayIgnorePattern: '^_',
				},
			],

			'@typescript-eslint/no-non-null-assertion': 'off',
		},
	},

	{
		files: ['**/*.astro/*.js', '*.astro/*.js'],

		languageOptions: {
			parser: tseslint.parser,
		},
	},
];
